/**
 * funcion que devuelve un elemento
 * de un array de forma aleatoria
 * @param  {array} palabras [array de elementos]]
 * @return {string}          [palabra a utilizar]
 */
function obtenerPalabra(palabras){
	let numero=Math.trunc(Math.random()*palabras.length);
	return palabras[numero];
}


function dibujar(palabra,selector){
	let caja=document.querySelector(selector);
	for (let i = 0; i < palabra.length; i++) {
		caja.innerHTML+="<div>&nbsp;</div>";
	}
}


  function comprobar(palabra,cletra,cajas,aciertos){
	  	let letra=(cletra.value=="") ? "&nbsp;" : cletra.value; // para evitar el descuadre del inline block
	  	let cErrores=document.querySelector("#errores");
	  	let acertar=0;
	  	cletra.value="";
	  		for (let i = 0; i < palabra.length; i++) {
	  			// has acertado
	  			if (palabra[i]==letra){
                    cajas[i].innerHTML=letra;
                    acertar=1;
                    aciertos+=1;
	  			}
	  			
	  		}
	  		// no has acertado
	  		if (!acertar) {
	  			cErrores.innerHTML+="<div>"+letra+"</div>";
	  			errores=dibujarError(errores);
	  			if (errores==0) {
	  			setTimeout(function(){
	  				alert("La liaste");
	  				location.reload();

	  			}, 200);
	  			}

	  		}else if (palabra.length==aciertos) {
	  			setTimeout(function(){
	  				alert("Ganaste");
	  				location.reload();

	  			}, 200);
	  			
	  		}
	  		return aciertos;
	  		
  }

  function dibujarError(errores){
  	let cajas=document.querySelector("#imagen");
	  	errores--;
	  	cajas.src="imgs/ahorcado_" + errores + ".png"

  	return errores;

  }